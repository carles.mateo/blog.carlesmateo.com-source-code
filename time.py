#!/usr/bin/env python3

#
# Date time methods
#
# Author: Carles Mateo
# Creation Date: 2019-11-20 17:23 IST
# Description: Class to return Date, datetime, Unix EPOCH timestamp
#

import datetime
import time


class DateTimeUtils:

    def get_unix_epoch(self):
        """
        Will return the EPOCH Time. For convenience is returned as String
        :return:
        """
        s_now_epoch = str(int(time.time()))

        return s_now_epoch

    def get_datetime(self, b_milliseconds=False):
        """
        Return the datetime with miliseconds in format YYYY-MM-DD HH:MM:SS.xxxxx
        or without milliseconds as YYYY-MM-DD HH:MM:SS"""
        if b_milliseconds is True:
            s_now = str(datetime.datetime.now())
        else:
            s_now = str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

        return s_now


def main():
    o_datetime = DateTimeUtils()
    s_now_epoch = o_datetime.get_unix_epoch()
    s_now_date = o_datetime.get_datetime()

    print(s_now_epoch)
    print(s_now_date)


main()
