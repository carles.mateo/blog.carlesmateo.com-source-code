
from first_unit_testing_example import *


def test_math_sum():

  assert math_sum(10, 5) == 15
  assert math_sum(-1, 1) == 0
  assert math_sum(1.1, 1) == 2.1
  assert math_sum(1.101, 0.01) == 1.111
  assert math_sum(1000, -10) == 990


def test_math_percent_simple():

  assert math_percent(1000, 10) == 1100
  assert math_percent(1000, -10) == 900


def test_math_percent_decimals():

  assert math_percent(1000.10, 10.01) == 1100.21
  assert math_percent(1000.10, 10.12) == 1101.31


def test_math_percent_rounding_decimals():

  # The 9.33% of 1111 is 1214.6563
  # The 9.3371% of 1111 is 1214.735181
  assert math_percent(1111, 9.33) == 1214.66
  assert math_percent(1111, 9.3371) == 1214.74


def test_return_number_pretty():

  assert isinstance(return_number_pretty(1000.0), str)
  assert return_number_pretty(1000.0) == "1000"
  assert return_number_pretty(1000.1999) == "1000.1999"
  assert return_number_pretty(-1000.19) == "-1000.19"
  assert return_number_pretty(-1000.0) == "-1000"
  assert return_number_pretty(0.0) == "0"


def test_main(capsys):
  """
  Test main() function
  :return:
  """

  s_expected_output = """The 10% of 1000 is 1100
The 10.1% of 1111 is 1223.21
The 9.33% of 1111 is 1214.66
The 9.3371% of 1111 is 1214.74"""

  main()

  o_captured = capsys.readouterr()
  assert o_captured.out == s_expected_output + "\n"
  assert o_captured.err == ""
