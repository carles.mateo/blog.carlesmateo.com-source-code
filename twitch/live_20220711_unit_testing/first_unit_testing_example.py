
def math_sum(f_number1, f_number2):
    """
    Returns the sum of two numbers (integers or floats)
    :param f_number1:
    :param f_number2:
    :return: float: The result
    """
    return f_number1 + f_number2

def math_percent(i_number, f_percent):
    """
    Given a number, return it with the percent requested summed.
    And with two decimal positions at maximum.
    For example 1000 10 will return 1100.00
    :param i_number:
    :param f_percent: Percentage in the form 0 or 0.1 or 10 or 22.5 for example
    :return: float: The result
    """

    f_result = round(i_number + i_number * f_percent/100, 2)

    return f_result


def return_number_pretty(f_number):
    """
    Will print a number in a pretty format.
    Ie: 1100.0 will be return 1100
    :param f_number:
    :return: string: the number without the decimal part if it had .0 at the end
    """

    if f_number == int(f_number):
        f_number = int(f_number)

    return str(f_number)


def main():
    # i_result = math_sum(10, 5)
    # print("10 + 5 =", i_result)

    a_values = [(1000, 10),
                (1111, 10.1),
                (1111, 9.33),
                (1111, 9.3371)]

    for t_values in a_values:

        i_base = t_values[0]
        f_percent = t_values[1]
        f_result = math_percent(i_base, f_percent)
        f_result_nice = return_number_pretty(f_result)
        print("The " + str(f_percent) + "% of " + str(i_base) + " is", f_result_nice)


if __name__ == "__main__":
    main()
