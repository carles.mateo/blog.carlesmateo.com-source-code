from turtle import *
from random import randint


class CommanderTurle:

    s_VERSION = "0.1"

    def __init__(self):
        self.i_pen_thickness = 1

    def print_help(self):
        """
        Prints help
        :return:
        """
        s_title = "Commander Turtle v. " + self.s_VERSION
        print(s_title)
        print("=" * len(s_title))
        print()
        print("Input your commands / Introdueix les teves comandes:")
        print("A or A199 - Advance / Avança")
        print("R or R180 - Rotate / Rota")
        print("L2 or L50 - Loop the next commands / Repeteix les següents comandes")
        print("C or CRED - Random color or Color Red / Color aleatori o color vermell")
        print("P or P10  - Pen thickness or 10 / Gruix del llapis de 10")
        print("Q - Quit / Sortir")
        print()
        print("Try/Intenta: ")
        print("L360,C,A1,R1")
        print("L4,C,A400,R90")
        print("L100,C,R91,A200")
        print()

    def ask_commands_from_keyboard(self):
        """
        Ask the user to enter the commands
        :return: String with the commands
        """

        while True:
            s_command = input(":")
            if s_command == "":
                print("Please enter a command / Si us plau introdueix una comanda")
                continue
            break

        return s_command

    def get_commands(self, s_command):
        """
        Breaks a command or sequence of commands in an array of commands
        :return:
        """
        # Asks user to input a command
        a_commands = s_command.split(",")

        s_first_command = a_commands[0]
        if len(s_first_command) > 0:
            if s_first_command[0] == "L":
                # Capture the number of times we want to loop
                i_times_to_repeat = 1
                if len(s_first_command) > 1:
                    i_times_to_repeat = int(s_first_command[1:])

                a_commands.remove(s_first_command)
                a_commands_new = a_commands * i_times_to_repeat

                a_commands = a_commands_new

        return a_commands

    def main(self):
        self.print_help()
        bgcolor("black")
        speed(0)

        b_exit = False
        while b_exit is False:
            s_commands = o_commander_turtle.ask_commands_from_keyboard()
            a_commands = o_commander_turtle.get_commands(s_commands)

            for s_single_command in a_commands:
                pensize(self.i_pen_thickness)

                s_single_command = s_single_command.upper()
                if s_single_command[0] == "A":
                    s_avanca = s_single_command[1:]
                    i_avanca = int(s_avanca) + 1

                    fd(i_avanca)

                if s_single_command[0] == "R":
                    if len(s_single_command) > 1:
                        s_rota = s_single_command[1:]
                    else:
                        s_rota = 90
                    i_rota = int(s_rota)

                    rt(i_rota)

                if s_single_command[0] == "C":
                    if len(s_single_command) == 1:
                        # Random color
                        i_r = randint(0, 255)
                        i_g = randint(0, 255)
                        i_b = randint(0, 255)

                        colormode(255)
                        pencolor(i_r, i_g, i_b)

                    if s_single_command == "CRED":
                        i_r = 255
                        i_g = 0
                        i_b = 0

                        colormode(255)
                        pencolor(i_r, i_g, i_b)

                if s_single_command[0] == "P":
                    if len(s_single_command) == 1:
                        self.i_pen_thickness += 1
                    else:
                        self.i_pen_thickness = int(s_single_command[1:])

                if s_single_command[0] == "Q":
                    b_exit = True
                    break

        print("Press on the image to close it")
        exitonclick()


if __name__ == "__main__":

    o_commander_turtle = CommanderTurle()
    o_commander_turtle.main()


