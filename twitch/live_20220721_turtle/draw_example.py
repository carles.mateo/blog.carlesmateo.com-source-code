from turtle import *
from random import randint


bgcolor("black")
i_x = 1
speed(0)

while i_x < 900:
    i_r = randint(0,255)
    i_g = randint(0,255)
    i_b = randint(0,255)

    colormode(255)
    pencolor(i_r, i_g, i_b)
    fd(50 + i_x)
    rt(90.991)
    i_x = i_x + 1

exitonclick()
