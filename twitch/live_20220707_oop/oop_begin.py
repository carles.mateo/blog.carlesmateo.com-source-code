class Employee:

    WIDE_COLUMN_NAME = 9
    WIDE_COLUMN_CITY = 24
    WIDE_COLUMN_SALARY = 8

    def __init__(self, s_name, s_city, i_salary, s_date_started):
        self.s_name_employee = s_name
        self.s_city_employee = s_city
        self.i_salary_employee = i_salary
        self.s_date_started_employee = s_date_started

    @staticmethod
    def print_header():
        print("Name".ljust(Employee.WIDE_COLUMN_NAME),
              "City".rjust(Employee.WIDE_COLUMN_CITY),
              "Salary".rjust(Employee.WIDE_COLUMN_SALARY))
        print("=" * (Employee.WIDE_COLUMN_NAME) + " " + "=" * (Employee.WIDE_COLUMN_CITY) + " " + "=" * (Employee.WIDE_COLUMN_SALARY))

    def print_employee_information(self):
        """
        Prints information about the employee
        :return:
        """
        print(self.s_name_employee.ljust(Employee.WIDE_COLUMN_NAME),
              self.s_city_employee.rjust(Employee.WIDE_COLUMN_CITY),
              str(self.i_salary_employee).rjust(Employee.WIDE_COLUMN_SALARY))

    def increase_salary(self, f_percentage):
        """
        Increase the salary of the Employee and stores as Integer
        :param f_percentage: For example 10.5
        :return:
        """
        self.i_salary_employee = int(self.i_salary_employee * f_percentage)


if __name__ == "__main__":

    # Print the header
    Employee.print_header()

    o_edu = Employee("Edu", "Barcelona", 200001, "2001-01-01")
    o_maria = Employee("Maria", "Cork", 120007, "1999-09-09")
    o_sid = Employee("Sid", "London", 90055, "1997-07-05")
    o_michela = Employee("Michela", "Dublin", 110177, "1996-05-09")
    o_txell = Employee("Txell", "Sant Feliu de Llobregat", 99777, "1995-01-02")

    a_employees = [o_edu, o_maria, o_sid, o_michela, o_txell]

    i_pointer = 0
    while i_pointer < len(a_employees):

        o_employee = a_employees[i_pointer]
        # Increase the salary 10%
        o_employee.increase_salary(10.5)

        o_employee.print_employee_information()

        i_pointer = i_pointer + 1
