import time


class ExhaustMemory:
    """
    Will allocate memory
    """

    s_program_name = "exhaustmemory"
    s_program_version = "0.1"

    def print_version(self):
        print(self.s_program_name, self.s_program_version)

    def allocate_memory(self, i_bytes):
        """
        Allocates memory in the form of a ByteArray of the size bytes specified
        :param i_bytes:
        :return: Boolean for success, bytearray
        """
        b_successfully_allocated = True
        try:
            by_allocated = bytearray(i_bytes)
        except:
            b_successfully_allocated = False

        return b_successfully_allocated, by_allocated

    def main(self):
        self.print_version()

        a_blocks_allocated = []

        i_bytes_in_one_kbyte = 1024
        i_bytes_in_one_mbyte = 1024 * i_bytes_in_one_kbyte

        i_bytes_to_allocate = 64 * i_bytes_in_one_mbyte

        try:

            i_counter = 0
            i_allocated_mb = 0
            while True:

                i_mb_to_allocate = int(i_bytes_to_allocate / i_bytes_in_one_mbyte)
                print("Allocating", i_mb_to_allocate, "MB")
                b_success, by_mem = self.allocate_memory(i_bytes_to_allocate)

                if b_success is False:
                    print("Memory allocation failed! Exiting")
                    break

                i_allocated_mb = i_allocated_mb + i_mb_to_allocate

                # Keep the memory allocated so it is not freed
                a_blocks_allocated.append(by_mem)

                if i_counter > 9:
                    i_counter = 0
                    print("Allocated so far:", i_allocated_mb, "MB")
                    # Sleep is not needed by me, is needed for you human, to be able to observe your system resources and to be able to
                    # have enough time to decide if cancelling or continuing. Human brains are slow! :)
                    print("Sleeping for 1 second")
                    time.sleep(1)
                else:
                    i_counter += 1

        except KeyboardInterrupt:
            print("CTRL + C pressed. Exiting")

        except:
            print("Problems allocating memory. Exiting")


if __name__ == "__main__":

    o_exhaustmemory = ExhaustMemory()
    o_exhaustmemory.main()
