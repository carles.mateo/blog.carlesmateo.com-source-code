from exhaustmemory import ExhaustMemory


class TestExhaustMemory:

    def test_print_version(self, capsys):
        o_exhaustversion = ExhaustMemory()
        o_exhaustversion.print_version()

        o_captured = capsys.readouterr()
        assert o_captured.out == "exhaustmemory 0.1\n"
        assert o_captured.err == ""

    def test_allocate_memory(self):
        o_exhaustversion = ExhaustMemory()

        # Allocate just one byte
        b_success, ba_allocated = o_exhaustversion.allocate_memory(1)

        assert b_success is True
        assert len(ba_allocated) == 1
