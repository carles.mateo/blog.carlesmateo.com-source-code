#!/bin/env python
# Collection of Effects
# 1m - Bold
# 2m - Normal Dark colors
# 3m - Italic
# 4m - Underline
# 7m - Background
# 9m - Strikethrough (except 38;9m)
# 40m - Bakground Dark Grey, with the colors in foreground 1 (bold),2,31-37
# 41m - Bakground Red, with the colors in foreground 1 (bold),2,30-37
# 42m - Bakground Green, with the colors in foreground 1 (bold),2,30-37
# 43m - Bakground Yellow, with the colors in foreground 1 (bold),2,30-37
# 44m - Bakground Blue, with the colors in foreground 1 (bold),2,30-37
# 45m - Bakground Violet, with the colors in foreground 1 (bold),2,30-37
# 46m - Bakground Cyan, with the colors in foreground 1 (bold),2,30-37
# 47m - Bakground Grey, with the colors in foreground 1 (bold),2,31-37
# 49m - Normal

CLEAR = "\x1B[0m"

a_s_background = ["1", "2", "3", "4", "7", "9", "40", "41", "42", "43", "44", "45", "46", "47", "49"]
a_s_color = ["1", "2", "31", "32", "33", "34", "35", "36", "37"]

for s_effect in a_s_background:
    s_line = ""
    for s_color in a_s_color:
        s_color_text = "\x1B[" + s_color + ";" + s_effect + "m"
        s_line += s_color_text + s_color + ";" + s_effect + "m" + CLEAR + " "
    print(s_line)
    print("--------------------------------------------")
