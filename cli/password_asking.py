#!/usr/bin/env python3
import getpass

# Prompt for a password, with echo turned off.
s_password = getpass.getpass("Password:")

print("Pssstttt... don't tell anyone, your password is:", s_password)
