#!/usr/bin/env python3
import getpass
import telnetlib

s_host = "localhost"
s_user = "telnet"
s_password = "telnet"
# s_user = input("Enter your remote account: ")

o_tn = telnetlib.Telnet(s_host)

o_tn.read_until(b"login: ")
o_tn.write(s_user.encode('ascii') + b"\n")

o_tn.read_until(b"Password: ")
# s_password = getpass.getpass()

o_tn.write(s_password.encode('ascii') + b"\n")

o_tn.write(b"hostname\n")
o_tn.write(b"uname -a\n")
o_tn.write(b"ls -hal /\n")
o_tn.write(b"df -h /\n")
o_tn.write(b"exit\n")

print(o_tn.read_all().decode('ascii'))

