#!/usr/bin/env bash

# https://gitlab.com/carles.mateo/blog.carlesmateo.com-source-code.git
# In one line:i_counter=0; for s_filename in `find . -name "*.py"`; do wc --lines $s_filename; i_num=`cat $s_filename ¦ wc --lines`; i_counter=$((i_counter + i_num)); done; echo "Total lines: $i_counter"

s_mask="*.py"
s_path="."

i_counter=0
i_files=0

for s_filename in $(find . -name "$s_mask"); do
  i_files=$((i_files+1))
  # Using modern way $() instead of `` https://github.com/koalaman/shellcheck/wiki/SC2006
  i_num=$(cat "$s_filename" | wc --lines)
  i_counter=$((i_counter+i_num))
  echo "$i_files $i_num $s_filename"
done

echo "Total lines: $i_counter"
echo "Total files: $i_files with mask: $s_mask in path: $s_path"
