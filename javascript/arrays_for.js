// Easy case
const a_i_temps = [];
a_i_temps[0] = 10;
a_i_temps[1] = 25;
a_i_temps[2] = 11;

a_i_temps.sort();
i_temp_lectures = a_i_temps.length;

for (let i_index = 0; i_index < i_temp_lectures; i_index++) {
    console.log(a_i_temps[i_index]);
}

for (let i_temp of a_i_temps) {
  console.log(i_temp);
}

// console.log(typeof a_i_temps);
// console.log(Array.isArray(a_i_temps));

// Difficult case
// Note: in Javascript this is not an Array, is a named index and in Javascript is an Object
const d_i_population = [];
d_i_population['Barcelona'] = 1500000;
d_i_population['Cork'] = 285000;

// Be aware this returns 0!
console.log(d_i_population.length);
// Note: It is not an Array, but it says it is
// console.log(typeof d_i_population);
// console.log(Array.isArray(d_i_population));

const a_population = d_i_population.entries();
for (let i_citizen of a_population) {
  console.log(i_citizen);
}